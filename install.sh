


# add mosek.jar
PATH_MOSEK="/Users/thibautdurand/mosek/7/tools/platform/osx64x86/bin/"
echo "@ add mosek.jar"
mvn install:install-file -Dfile=${PATH_MOSEK}/mosek.jar -DgroupId=com.mosek -DartifactId=mosek7 -Dversion=1.0 -Dpackaging=jar
echo "@ add progressbar"
mvn install:install-file -Dfile=extern_jar/progressbar-0.0.1-SNAPSHOT.jar -DgroupId=fr.durandt -DartifactId=progressbar -Dversion=0.0.1-SNAPSHOT -Dpackaging=jar

echo "@ generate package"
mvn package

echo "@ generate source-jar"
mvn source:jar

