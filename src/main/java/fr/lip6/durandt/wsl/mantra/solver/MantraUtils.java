package fr.lip6.durandt.wsl.mantra.solver;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import fr.lip6.durandt.wsl.mantra.model.IMantraModel;
import fr.lip6.durandt.wsl.mantra.model.MantraLatent;
import fr.lip6.durandt.wsl.mantra.model.PrimalMantraModel;
import fr.lip6.durandt.wsl.util.LabelLatent;
import fr.lip6.durandt.wsl.util.LabeledObject;

/**
 * Class to compute standard LSSVM operations: <br/>
 * - primal objective <br/>
 * - empirical loss <br/>
 * - loss  
 * 
 * @author Thibaut Durand - durand.tibo@gmail.com
 *
 * @param <X>
 * @param <Y>
 * @param <H>
 */
public class MantraUtils<X, Y, H> {

	private int nThreads = 1;

	public MantraUtils() {
		this(1);
	}

	public MantraUtils(int nThreads) {
		super();
		this.nThreads = nThreads;
	}

	/**
	 * 1/n sum_i=1^n delta(yi, yibar, hibar) where (yibar, hibar) = max_(y,h) w^T psi(xi,y,h)
	 * @param model
	 * @param data
	 * @return 1/n sum_i=1^n delta(yi, yibar, hibar)
	 */
	public double empiricalLoss(final IMantraModel<X, Y, H> model, 
			final List<LabeledObject<X, Y>> data) {

		double loss = 0;

		if(nThreads > 1) { // Multi-threads execution
			ExecutorService executor = Executors.newFixedThreadPool(nThreads);
			List<Future<Double>> futures = new ArrayList<Future<Double>>();
			CompletionService<Double> completionService = new ExecutorCompletionService<Double>(executor);

			for(int i=0 ; i<data.size(); i++) {
				final int n = i;
				futures.add(completionService.submit(new Callable<Double>() {

					public Double call() throws Exception {
						LabeledObject<X, Y> ls = data.get(n);
						LabelLatent<Y, MantraLatent<H>> prediction = model.predict(ls.getPattern());
						Y yPredict = prediction.getLabel();
						MantraLatent<H> hPredict = prediction.getLatent();
						return model.loss(ls.getLabel(), yPredict, hPredict);
					}
				}));
			}

			for(Future<Double> f : futures) {
				try {
					Double res = f.get();
					if (res != null) {
						loss += res;
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
			}
			executor.shutdown();
		}
		else { // Mono-thread execution
			for(LabeledObject<X, Y> ls : data) {
				LabelLatent<Y, MantraLatent<H>> prediction = model.predict(ls.getPattern());
				Y yPredict = prediction.getLabel();
				MantraLatent<H> hPredict = prediction.getLatent();
				loss += model.loss(ls.getLabel(), yPredict, hPredict);
			}
		}
		loss /= (double)data.size();
		return loss;
	}

	public int getnThreads() {
		return nThreads;
	}

	/**
	 * 1/n sum_i=1^n max_(y,h) delta(yi,y,h) + w^T psi(xi,y,h) - max_hstar w^T psi(xi,yi,hstar)	
	 * @param model
	 * @param data
	 * @return
	 */
	public double loss(final PrimalMantraModel<X, Y, H> model,
			final List<LabeledObject<X, Y>> data) {

		double loss = 0;

		if(nThreads > 1) { // Multi-threads execution

			ExecutorService executor = Executors.newFixedThreadPool(nThreads);
			List<Future<Double>> futures = new ArrayList<Future<Double>>();
			CompletionService<Double> completionService = new ExecutorCompletionService<Double>(executor);

			for(int i=0 ; i<data.size(); i++) {
				final int n = i;
				futures.add(completionService.submit(new Callable<Double>() {
					public Double call() throws Exception {
						// select example n
						LabeledObject<X, Y> lo = data.get(n);
						X x = lo.getPattern();
						Y y = lo.getLabel();

						// computes the loss-augmented inference 
						LabelLatent<Y, MantraLatent<H>> prediction = model.maxOracle(lo.getPattern(), lo.getLabel());
						Y yPredict = prediction.getLabel();
						MantraLatent<H> hPredict = prediction.getLatent();

						// computes the latent prediction for the true label
						MantraLatent<H> hBar = model.predict(x, y);

						return model.loss(lo.getLabel(), yPredict, hPredict) 
								+ model.valueOf(lo.getPattern(), yPredict, hPredict) 
								- model.valueOf(lo.getPattern(), y, hBar);
					}
				}));
			}

			for(Future<Double> f : futures) {
				try {
					Double res = f.get();
					if (res != null) {
						loss += res;
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
			}
			executor.shutdown();
		}
		else { // Mono-thread execution
			for(LabeledObject<X, Y> lo : data) {
				X x = lo.getPattern();
				Y y = lo.getLabel();

				// computes the loss-augmented inference 
				LabelLatent<Y, MantraLatent<H>> prediction = model.maxOracle(lo.getPattern(), lo.getLabel());
				Y yPredict = prediction.getLabel();
				MantraLatent<H> hPredict = prediction.getLatent();

				// computes the latent prediction for the true label
				MantraLatent<H> hBar = model.predict(x, y);

				loss += model.loss(lo.getLabel(), yPredict, hPredict) 
						+ model.valueOf(lo.getPattern(), yPredict, hPredict) 
						- model.valueOf(lo.getPattern(), y, hBar);
			}
		}
		loss /= data.size();

		return loss;	
	}

	/**
	 * lambda/2 ||w||^2 + c/n sum_i=1^n max_(y,h) delta(yi,y,h) + w^T psi(xi,y,h) - max_hstar w^T psi(xi,yi,hstar)
	 * @param data
	 * @param instantiation
	 * @param model
	 * @param lambda
	 * @param nThreads
	 * @return
	 */
	public double primalObjective(final PrimalMantraModel<X, Y, H> model,
			final List<LabeledObject<X, Y>> data,
			final double lambda,
			final double c) {

		double loss = loss(model, data);
		return 0.5 * lambda * model.norm2() + c * loss;	
	}

	public void setnThreads(int nThreads) {
		this.nThreads = nThreads;
	}

}
