package fr.lip6.durandt.wsl.mantra.model;

import java.io.Serializable;
import java.util.Arrays;

public class MantraLatent<H> implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 4565242154020426733L;

	private H hMax;

	private H hMin;

	public MantraLatent(H hMax, H hMin) {
		super();
		this.hMax = hMax;
		this.hMin = hMin;
	}

	public H gethMax() {
		return hMax;
	}

	public void sethMax(H hMax) {
		this.hMax = hMax;
	}

	public H gethMin() {
		return hMin;
	}

	public void sethMin(H hMin) {
		this.hMin = hMin;
	}

	@Override
	public String toString() {
		// get the class of Y
		Class<? extends Object> classH = hMax.getClass();
		if(classH == (new Integer[0]).getClass()) {
			return "MantraLatent [hMax=" + Arrays.toString((Integer[])hMax) + ", hMin=" + Arrays.toString((Integer[])hMin) + "]";
		}
		return "MantraLatent [hMax=" + hMax + ", hMin=" + hMin + "]";
	}



}
