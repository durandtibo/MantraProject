package fr.lip6.durandt.wsl.mantra.model;

import java.util.ArrayList;
import java.util.List;

import fr.lip6.durandt.wsl.util.LabelLatent;
import fr.lip6.durandt.wsl.util.LabeledObject;
import fr.lip6.durandt.wsl.util.data.bag.Bag;

public class MultiClassMantraModel4Bag<X extends Bag<Z>, Z> extends PrimalMantraModel<X, Integer, Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8502330546678488328L;

	private int numClasses = 0;

	private String initLatentType = "fixe";

	@Override
	public void add(double[] a, X x, Integer y, MantraLatent<Integer> h) {
		double[] psiMax = x.getInstanceRepresentation(h.gethMax());
		double[] psiMin = x.getInstanceRepresentation(h.gethMin());
		int offset = y * psiMax.length;
		synchronized(a) {
			for(int i=0; i<psiMax.length; i++) {
				a[offset+i] += (psiMax[i] + psiMin[i]);
			}
		}
	}

	@Override
	public void add(double[] a, X x, Integer y, MantraLatent<Integer> h, double gamma) {
		double[] psiMax = x.getInstanceRepresentation(h.gethMax());
		double[] psiMin = x.getInstanceRepresentation(h.gethMin());
		int offset = y * psiMax.length;
		synchronized(a) {
			for(int i=0; i<psiMax.length; i++) {
				a[offset+i] += (psiMax[i] + psiMin[i]) * gamma;
			}
		}
	}

	public List<Integer[]> computePrediction(List<LabeledObject<X, Integer>> data) {
		List<Integer[]> predictionsAndLabels = new ArrayList<Integer[]>();
		for(LabeledObject<X, Integer> example : data) {
			Integer[] p = new Integer[2];
			p[0] = predict(example.getPattern()).getLabel();
			p[1] = example.getLabel();
			predictionsAndLabels.add(p);
		}
		return predictionsAndLabels;
	}

	@Override
	public double[] featureMap(X x, Integer y, MantraLatent<Integer> h) {
		double[] featureMax = x.getInstanceRepresentation(h.gethMax());
		double[] featureMin = x.getInstanceRepresentation(h.gethMin());	
		double[] psi = new double[featureMax.length * numClasses];
		int offset = y * featureMax.length;
		for(int i=0; i<featureMax.length; i++) {
			psi[offset+i] = featureMax[i] + featureMin[i];
		}
		return psi;
	}

	@Override
	public int getDimension(X x, Integer y, MantraLatent<Integer> h) {
		return numClasses * x.getInstanceRepresentation(0).length;
	}

	public String getInitLatentType() {
		return initLatentType;
	}

	public int getNumClasses() {
		return numClasses;
	}

	@Override
	public void initialization(List<LabeledObject<X, Integer>> data) {
		if (numClasses == 0) {
			// search the number of classes
			for(int i=0; i<data.size(); i++) {
				numClasses = Math.max(numClasses, data.get(i).getLabel());
			}
			numClasses++;
		}
	}

	@Override
	public MantraLatent<Integer> initializeLatent(X x, Integer y) {
		if (initLatentType.compareToIgnoreCase("fixe") == 0) {
			// initialize with the first element of the bag
			return new MantraLatent<Integer>(0, 0);	
		}
		return null;
	}

	@Override
	public double loss(Integer yTruth, Integer yPredict, MantraLatent<Integer> hPredict) {
		if (yTruth == yPredict) {
			return 0.0;
		}
		else {
			return 1.0;
		}
	}

	@Override
	public LabelLatent<Integer, MantraLatent<Integer>> maxOracle(X x, Integer yStar) {
		long tStart = System.currentTimeMillis();

		if(numClasses < 2) {
			throw new IllegalArgumentException("numClasses must be >0");
		}

		Integer yPredict = null;
		MantraLatent<Integer> hPredict = null;
		double valmax = -Double.MAX_VALUE;
		for(int y=0; y<numClasses; y++) {
			MantraLatent<Integer> h = predict(x, y);
			double val = loss(yStar, y, h) + valueOf(x, y, h);
			if(val>valmax){
				valmax = val;
				yPredict = y;
				hPredict = h;
			}
		}

		long tEnd = System.currentTimeMillis();
		tMaxOracle += (tEnd - tStart);

		return new LabelLatent<Integer, MantraLatent<Integer>>(yPredict, hPredict);
	}

	@Override
	public LabelLatent<Integer, MantraLatent<Integer>> predict(X x) {
		long tStart = System.currentTimeMillis();

		if(numClasses < 2) {
			throw new IllegalArgumentException("numClasses must be >0");
		}

		Integer yPredict = null;
		MantraLatent<Integer> hPredict = null;
		double valmax = -Double.MAX_VALUE;
		for(int y=0; y<numClasses; y++) {
			MantraLatent<Integer> h = predict(x, y);
			double val = valueOf(x, y, h);
			if(val>valmax){
				valmax = val;
				yPredict = y;
				hPredict = h;
			}
		}

		long tEnd = System.currentTimeMillis();
		tPredict += (tEnd - tStart);

		return new LabelLatent<Integer, MantraLatent<Integer>>(yPredict, hPredict);
	}

	@Override
	public MantraLatent<Integer> predict(X x, Integer y) {

		Integer hMax = null;
		Integer hMin = null;
		double valmax = -Double.MAX_VALUE;
		double valmin = Double.MAX_VALUE;

		for(int h=0; h<x.getNumberOfInstances(); h++) {
			double val = valueOf(x, y, h);
			if(val>valmax){
				valmax = val;
				hMax = h;
			}
			if(val<valmin){
				valmin = val;
				hMin = h;
			}
		}

		return new MantraLatent<Integer>(hMax, hMin);
	}

	public void setInitLatentType(String initLatentType) {
		this.initLatentType = initLatentType;
	}

	public void setNumClasses(int numClasses) {
		this.numClasses = numClasses;
	}

	@Override
	public void sub(double[] a, X x, Integer y, MantraLatent<Integer> h) {
		double[] psiMax = x.getInstanceRepresentation(h.gethMax());
		double[] psiMin = x.getInstanceRepresentation(h.gethMin());
		int offset = y * psiMax.length;
		synchronized(a) {
			for(int i=0; i<psiMax.length; i++) {
				a[offset+i] -= (psiMax[i] + psiMin[i]);
			}
		}
	}

	private double valueOf(X x, Integer y, Integer h) {
		double[] feature = x.getInstanceRepresentation(h);
		double[] w = getWeights();
		double score = 0;
		int offset = y * feature.length;
		for(int i=0; i<feature.length; i++) {
			score += feature[i] * w[i+offset];
		}
		return score;
	}

	@Override
	public double valueOf(X x, Integer y, MantraLatent<Integer> h) {
		double[] psiMax = x.getInstanceRepresentation(h.gethMax());
		double[] psiMin = x.getInstanceRepresentation(h.gethMin());	
		int offset = y * psiMax.length;
		double[] w = getWeights();
		double score = 0;
		for(int i=0; i<psiMax.length; i++) {
			score += (psiMax[i] + psiMin[i]) * w[i+offset];
		}
		return score;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " [numClasses=" + numClasses + ", initLatentType=" + initLatentType + ", dimension=" + getDimension() + "]";
	}


}
