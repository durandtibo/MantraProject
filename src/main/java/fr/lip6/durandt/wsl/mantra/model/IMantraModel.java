package fr.lip6.durandt.wsl.mantra.model;

import java.io.Serializable;
import java.util.List;

import fr.lip6.durandt.wsl.util.LabelLatent;
import fr.lip6.durandt.wsl.util.LabeledObject;

public interface IMantraModel<X, Y, H> extends Serializable {

	public double loss(Y yTruth, Y yPredict, MantraLatent<H> hPredict);

	public LabelLatent<Y, MantraLatent<H>> maxOracle(X x, Y yStar);

	public LabelLatent<Y, MantraLatent<H>> predict(X x);

	public MantraLatent<H> predict(X x, Y y);

	public MantraLatent<H> initializeLatent(X x, Y y);

	public void initialization(List<LabeledObject<X, Y>> data);
	
}
