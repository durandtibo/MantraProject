package fr.lip6.durandt.wsl.mantra.solver;

import fr.lip6.durandt.wsl.mantra.model.MantraLatent;
import fr.lip6.durandt.wsl.mantra.model.PrimalMantraModel;
import fr.lip6.durandt.wsl.util.LabelLatent;
import fr.lip6.durandt.wsl.util.solver.StructuralLoss;

public class MantraLoss2<X, Y, H> implements StructuralLoss<X, LabelLatent<Y, MantraLatent<H>>, PrimalMantraModel<X, Y, H>> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6539845266633172081L;

	@Override
	public void add(PrimalMantraModel<X, Y, H> model, double[] a, X x, LabelLatent<Y, MantraLatent<H>> y) {
		model.add(a, x, y);
	}

	@Override
	public void add(PrimalMantraModel<X, Y, H> model, double[] a, X x, LabelLatent<Y, MantraLatent<H>> y, double gamma) {
		model.add(a, x, y, gamma);
	}

	@Override
	public double[] computeGradient(PrimalMantraModel<X, Y, H> model, X x, LabelLatent<Y, MantraLatent<H>> y) {
		// compute the latent prediction for the class y
		MantraLatent<H> hBar = model.predict(x, y.getLabel());
		// compute the loss augmented inference
		LabelLatent<Y, MantraLatent<H>> yStar = model.maxOracle(x, y);
		// compute the gradient of the loss
		double[] gradient = model.featureMap(x, yStar);
		model.sub(gradient, x, y.getLabel(), hBar);
		return gradient;
	}

	@Override
	public double[] computeGradient(PrimalMantraModel<X, Y, H> model, X x, LabelLatent<Y, MantraLatent<H>> y, LabelLatent<Y, MantraLatent<H>> yStar) {
		// compute the latent prediction for the class y
		MantraLatent<H> hBar = model.predict(x, y.getLabel());
		// compute the gradient of the loss
		double[] gradient = model.featureMap(x, yStar);
		model.sub(gradient, x, y.getLabel(), hBar);
		return gradient;
	}

	@Override
	public double error(PrimalMantraModel<X, Y, H> model, LabelLatent<Y, MantraLatent<H>> yTruth, LabelLatent<Y, MantraLatent<H>> y) {
		return model.loss(yTruth, y);
	}

	@Override
	public double evaluate(PrimalMantraModel<X, Y, H> model, X x, LabelLatent<Y, MantraLatent<H>> y) {
		// compute the latent prediction for the class y
		MantraLatent<H> hBar = model.predict(x, y.getLabel());
		// compute the loss augmented inference
		LabelLatent<Y, MantraLatent<H>> yStar = model.maxOracle(x, y);
		// compute the loss term
		return model.loss(y, yStar) + model.valueOf(x, yStar.getLabel(), yStar.getLatent()) - model.valueOf(x, y.getLabel(), hBar);
	}

	@Override
	public int getDimension(PrimalMantraModel<X, Y, H> model, X x, LabelLatent<Y, MantraLatent<H>> y) {
		return model.getDimension(x, y);
	}

	@Override
	public LabelLatent<Y, MantraLatent<H>> maxOracle(PrimalMantraModel<X, Y, H> model, X x, LabelLatent<Y, MantraLatent<H>> yStar) {
		return model.maxOracle(x, yStar);
	}

	@Override
	public void sub(PrimalMantraModel<X, Y, H> model, double[] a, X x, LabelLatent<Y, MantraLatent<H>> y) {
		model.sub(a, x, y);
	}

}
