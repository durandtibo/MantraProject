package fr.lip6.durandt.wsl.mantra.solver.ssg;

import java.util.ArrayList;
import java.util.List;

import fr.lip6.durandt.wsl.mantra.model.MantraLatent;
import fr.lip6.durandt.wsl.mantra.model.PrimalMantraModel;
import fr.lip6.durandt.wsl.mantra.solver.MantraLoss2;
import fr.lip6.durandt.wsl.mantra.solver.MantraSolver;
import fr.lip6.durandt.wsl.mantra.solver.MantraUtils;
import fr.lip6.durandt.wsl.util.LabelLatent;
import fr.lip6.durandt.wsl.util.LabeledObject;
import fr.lip6.durandt.wsl.util.solver.SolverOptions;
import fr.lip6.durandt.wsl.util.solver.ssg.SSGOptions;
import fr.lip6.durandt.wsl.util.solver.ssg.SSGSolver;

public class MantraWithSSG<X, Y, H> extends MantraSolver<X, Y, H> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3824925982563729251L;

	private SSGOptions options = null;

	public MantraWithSSG(SSGOptions options) {
		super();
		this.options = options;
	}

	public MantraWithSSG() {
		this(new SSGOptions());
	}

	@Override
	public SolverOptions getOptions() {
		return options;
	}

	@Override
	public PrimalMantraModel<X, Y, H> optimize(final PrimalMantraModel<X, Y, H> model, final List<LabeledObject<X, Y>> data) {

		List<LabeledObject<X, LabelLatent<Y, MantraLatent<H>>>> newData = new ArrayList<LabeledObject<X, LabelLatent<Y, MantraLatent<H>>>>(data.size());
		for(LabeledObject<X, Y> example : data) {
			LabelLatent<Y, MantraLatent<H>> newLabel = new LabelLatent<Y, MantraLatent<H>>(example.getLabel(), model.initializeLatent(example.getPattern(), example.getLabel()));
			newData.add(new LabeledObject<X, LabelLatent<Y, MantraLatent<H>>>(example.getPattern(), newLabel));
		}

		SSGSolver<X, LabelLatent<Y, MantraLatent<H>>, PrimalMantraModel<X, Y, H>> solver = new SSGSolver<X, LabelLatent<Y, MantraLatent<H>>, PrimalMantraModel<X, Y, H>>(new MantraLoss2<X, Y, H>(), options);
		solver.optimize(model, newData);
		return model;
	}

	@Override
	public double primalObjective(final PrimalMantraModel<X, Y, H> model, 
			final List<LabeledObject<X, Y>> data) {
		MantraUtils<X, Y, H> util = new MantraUtils<X, Y, H>();
		return util.primalObjective(model, data, options.getLambda(), 1.0);
	}

}
