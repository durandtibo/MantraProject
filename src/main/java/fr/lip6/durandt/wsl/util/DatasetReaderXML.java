package fr.lip6.durandt.wsl.util;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fr.durandt.progressbar.ProgressBar;
import fr.lip6.durandt.wsl.util.data.bag.DoubleBag;
import fr.lip6.durandt.wsl.util.data.bag.DoubleBagReader;

public class DatasetReaderXML implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4624845040140781106L;


	public static List<LabeledObject<DoubleBag, Integer>> readDoubleBag(File file, int dim, int maxNumberOfInstances, int verbose) {

		if (file == null) {
			throw new RuntimeException("file is not defined (null)");
		}
		else if (!file.exists()) {
			throw new RuntimeException("file does not exist (" + file.getAbsolutePath() + ")");
		}

		List<LabeledObject<DoubleBag, Integer>> list = null;

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		try {

			System.out.println("read file " + file.getAbsolutePath());

			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(file);
			Element racine = document.getDocumentElement();

			// Read the number of bags nbBags
			int nbBags = Integer.parseInt(racine.getAttribute("size"));

			// Initialize the list of bags
			list = new ArrayList<LabeledObject<DoubleBag, Integer>>(nbBags);
			int nbInstancesAll = 0;

			NodeList racineNodes = racine.getChildNodes();
			int nbRacineNodes = racineNodes.getLength();

			ProgressBar pb = new ProgressBar("read", nbBags, 50);
			if (maxNumberOfInstances != 0) pb.start();

			for(int i=0; i<nbRacineNodes; i++) {
				if(racineNodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
					Element e = (Element) racineNodes.item(i);
					if(e.getTagName().compareToIgnoreCase("example") == 0) {
						NodeList childNoeuds = e.getChildNodes();
						int nbChildNoeuds = childNoeuds.getLength();

						String featureFile = null;
						String name = null;
						int label = 0;

						for(int j=0; j<nbChildNoeuds; j++) {
							if(childNoeuds.item(j).getNodeType() == Node.ELEMENT_NODE) {
								Element childElement = (Element) childNoeuds.item(j);

								if(verbose > 2) System.out.println(j + "\t" + childElement);

								if(childElement.getTagName().compareToIgnoreCase("featurefile") == 0) {
									featureFile = childElement.getTextContent();
								}
								else if(childElement.getTagName().compareToIgnoreCase("imagename") == 0) {
									name = childElement.getTextContent();
								}
								else if(childElement.getTagName().compareToIgnoreCase("output") == 0) {
									label = Integer.parseInt(childElement.getTextContent());
								}
							}
						}

						DoubleBag bag = null;
						if(featureFile != null && maxNumberOfInstances != 0) {
							bag = DoubleBagReader.readXMLFile(new File(featureFile), dim, maxNumberOfInstances, 0);
						}
						if(name != null) {
							bag.setName(name);
						}

						// Add the new bag to the list
						list.add(new LabeledObject<DoubleBag, Integer>(bag, label));
						nbInstancesAll += bag.getNumberOfInstances();

						if (maxNumberOfInstances != 0) pb.step();

					}
				}
			}
			if (maxNumberOfInstances != 0) pb.stop();
			System.out.println("number of bags= " + list.size() + "\tnumber of instances= " + nbInstancesAll + "\taverage number of instances per bag= " + ((double)nbInstancesAll/(double)list.size()));
		}
		catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		catch (SAXException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}

		return list;
	}

}
