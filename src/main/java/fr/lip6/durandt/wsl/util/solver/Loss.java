package fr.lip6.durandt.wsl.util.solver;

import java.io.Serializable;

import fr.lip6.durandt.wsl.util.model.IPrimalModel;

public interface Loss<X, Y, M extends IPrimalModel> extends Serializable {

	public double[] computeGradient(M model, X x, Y y);

	public double evaluate(M model, X x, Y y);

	public int getDimension(M model, X x, Y y);
}
