package fr.lip6.durandt.wsl.util;

public class MatrixOps {

	/**
	 * a = a + b
	 * @param a
	 * @param b
	 * @return c
	 */
	public static void add(double[][] a, double[][] b) {
		for(int i=0; i<a.length; i++) {
			for(int j=0; j<a[i].length; j++) {
				a[i][j] = a[i][j] + b[i][j];
			}
		}
	}

	/**
	 * Performs a linear combination of 2 matrix and store the result in the first vector:
	 * a = gamma * a + lambda * b
	 * @param a
	 * @param b
	 * @param gamma
	 * @param lambda
	 */
	public static void add(double[][] a, double[][] b, double gamma, double lambda) {
		for(int i=0; i<a.length; i++) {
			for(int j=0; j<a[i].length; j++) {
				a[i][j] = gamma * a[i][j] + lambda * b[i][j];
			}
		}
	}

	/**
	 * c = a + lambda * b
	 * @param a
	 * @param lambda
	 * @param b
	 * @return c
	 */
	public static double[][] addn(double[][] a, double lambda, double[][] b) {
		double[][] c = new double[a.length][a[0].length];
		for(int i=0; i<a.length; i++) {
			for(int j=0; j<a[i].length; j++) {
				c[i][j] = a[i][j] + lambda * b[i][j];
			}
		}
		return c;
	}

	/**
	 * c = a + b
	 * @param a
	 * @param b
	 * @return c
	 */
	public static double[][] addn(double[][] a, double[][] b) {
		double[][] c = new double[a.length][a[0].length];
		for(int i=0; i<a.length; i++) {
			for(int j=0; j<a[i].length; j++) {
				c[i][j] = a[i][j] + b[i][j];
			}
		}
		return c;
	}

	/**
	 * Performs a linear combination of 2 matrix and store the result in the first vector:
	 * c = gamma * a + lambda * b
	 * @param a
	 * @param b
	 * @param gamma
	 * @param lambda
	 * @return c
	 */
	public static double[][] addn(double[][] a, double[][] b, double gamma, double lambda) {
		double[][] c = new double[a.length][a[0].length];
		for(int i=0; i<a.length; i++) {
			for(int j=0; j<a[i].length; j++) {
				c[i][j] = gamma * a[i][j] + lambda * b[i][j];
			}
		}
		return c;
	}


	/**
	 * c = a
	 * @param a
	 * @return c
	 */
	public static double[][] copy(double[][] a) {
		double[][] c = new double[a.length][a[0].length];
		for(int i=0; i<a.length; i++) {
			for(int j=0; j<a[i].length; j++) {
				c[i][j] = a[i][j];
			}
		}
		return c;
	}

	/**
	 * Compute the minimum of matrix a
	 * @param a
	 * @return
	 */
	public static int min(int[][] a) {
		int min = Integer.MAX_VALUE;
		for(int[] t : a) {
			for(int v : t) {
				if(v<min) {
					min = v;
				}
			}
		}
		return min;
	}

	/**
	 * a = a * lambda
	 * @param a
	 * @param lambda
	 */
	public static void mul(double[][] a, double lambda) {
		for(int i=0; i<a.length; i++) {
			for(int j=0; j<a[i].length; j++) {
				a[i][j] *= lambda;
			}
		}
	}

	/**
	 * c = a * lambda
	 * @param a
	 * @param lambda
	 * @return c
	 */
	public static double[][] muln(double[][] a, double lambda) {
		double[][] c = new double[a.length][a[0].length];
		for(int i=0; i<a.length; i++) {
			for(int j=0; j<a[i].length; j++) {
				c[i][j] = a[i][j] * lambda;
			}
		}
		return c;
	}

	/**
	 * alpha^T * Gramm * alpha
	 * @param alpha
	 * @param gram
	 * @return
	 */
	public static double product(double[] alpha, double[][] gram) {
		double[] tmp = new double[alpha.length];
		// tmp = gram * alpha
		for(int i=0; i<gram.length; i++) {
			tmp[i] = VectorOps.dot(gram[i],alpha);
		}
		double s = VectorOps.dot(alpha, tmp);
		return s;
	}

	/**
	 * a = a - b
	 * @param a
	 * @param b
	 * @return c
	 */
	public static void sub(double[][] a, double[][] b) {
		for(int i=0; i<a.length; i++) {
			for(int j=0; j<a[i].length; j++) {
				a[i][j] = a[i][j] - b[i][j];
			}
		}
	}

	/**
	 * c = a - b
	 * @param a
	 * @param b
	 * @return c
	 */
	public static double[][] subn(double[][] a, double[][] b) {
		double[][] c = new double[a.length][a[0].length];
		for(int i=0; i<a.length; i++) {
			for(int j=0; j<a[i].length; j++) {
				c[i][j] = a[i][j] - b[i][j];
			}
		}
		return c;
	}
}
