package fr.lip6.durandt.wsl.util.data;

import java.util.List;

public class Evaluation {

	public static double f1Score(List<Integer[]> predictionsAndLabels) {
		return fScore(predictionsAndLabels, 1);
	}

	public static double fScore(List<Integer[]> predictionsAndLabels, double beta) {
		double tp = 0;
		double fn = 0;
		double fp = 0;
		for(Integer[] p : predictionsAndLabels) {
			Integer prediction = p[0];
			Integer label = p[1];
			if (label == 1 && prediction == 1) tp++;
			if (label == 1 && prediction <= 0) fn++;
			if (label <= 0 && prediction == 1) fp++;
		}
		double p = tp / (tp + fp);
		double r = tp / (tp + fn);
		double f = (1 + beta * beta) * (p * r) / (beta * beta * p + r);
		if (Double.isNaN(f)) f = 0;
		System.out.println("fscore=" + f + "\ttp=" + tp + "\tfn=" + fn + "\tfp=" + fp + "\tp=" + p + "\tr=" + r);
		return f;
	}

	public static double multiClassAccuracy(List<Integer[]> predictionsAndLabels) {
		int numCorrects = 0;
		for(Integer[] p : predictionsAndLabels) {
			if (p[0] == p[1]) numCorrects++;
		}
		double accuracy = (double)numCorrects / (double)predictionsAndLabels.size();
		System.out.println("accuracy=" + accuracy + "\t number of correct predictions=" + numCorrects);
		return accuracy;
	}
}
