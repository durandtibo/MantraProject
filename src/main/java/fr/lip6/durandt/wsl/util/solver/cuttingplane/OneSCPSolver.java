package fr.lip6.durandt.wsl.util.solver.cuttingplane;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import fr.durandt.progressbar.ProgressBar;
import fr.lip6.durandt.wsl.util.LabeledObject;
import fr.lip6.durandt.wsl.util.MatrixOps;
import fr.lip6.durandt.wsl.util.MosekSolver;
import fr.lip6.durandt.wsl.util.VectorOps;
import fr.lip6.durandt.wsl.util.model.IPrimalModel;
import fr.lip6.durandt.wsl.util.solver.SolverUtils;
import fr.lip6.durandt.wsl.util.solver.StructuralLoss;

public class OneSCPSolver<X, Y, M extends IPrimalModel> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7408592731956526252L;

	private StructuralLoss<X, Y, M> loss = null;

	private OneSCPOptions options = null;

	final private double eps = 1e-8;

	public OneSCPSolver(StructuralLoss<X, Y, M> loss) {
		this(loss, new OneSCPOptions());
	}

	public OneSCPSolver(StructuralLoss<X, Y, M> loss, OneSCPOptions options) {
		super();
		this.loss = loss;
		this.options = options;
	}

	protected CuttingPlaneModel computeCuttingPlaneModel(
			final M model, 
			final List<LabeledObject<X, Y>> data) {

		final double[] at = new double[model.getDimension()];
		double bt = 0;
		final int n = data.size();

		for(int i=0; i<n; i++){
			// select example i
			LabeledObject<X, Y> example = data.get(i);			
			X x = example.getPattern();
			Y y = example.getLabel();

			// loss-augmented inference
			Y yStar = loss.maxOracle(model, x, y);

			// constant term of the cutting plane model
			bt += loss.error(model, y, yStar);

			// computes sub-gradient term of the cutting plane model
			//loss.add(model, at, x, yStar);
			//loss.sub(model, at, x, y);
			VectorOps.sub(at, loss.computeGradient(model, x, y, yStar));
		}

		// normalize by the number of examples
		if (n != 0) {
			VectorOps.mul(at, 1. /(double) n);
			bt /= n;
		}

		CuttingPlaneModel cuttingPlaneModel = new CuttingPlaneModel(at, bt);
		return cuttingPlaneModel;
	}

	/**
	 * Compute the cutting plane for the current model w
	 * @param data list of training samples
	 * @param model
	 * @param nThreads
	 * @return the cutting plane model
	 */
	protected CuttingPlaneModel computeCuttingPlaneModel(
			final M model, 
			final List<LabeledObject<X, Y>> data, 
			final int nThreads) {

		if(nThreads > 1) {
			return computeCuttingPlaneModelMultiThreads(model, data, nThreads);
		}
		else {
			return computeCuttingPlaneModel(model, data);
		}
	}

	protected CuttingPlaneModel computeCuttingPlaneModelMultiThreads(
			final M model, 
			final List<LabeledObject<X, Y>> data, 
			final int nThreads) {

		final double[] at = new double[model.getDimension()];
		double bt = 0;
		final int n = data.size();

		{
			ExecutorService executor = Executors.newFixedThreadPool(nThreads);
			List<Future<Double>> futures = new ArrayList<Future<Double>>();
			CompletionService<Double> completionService = new ExecutorCompletionService<Double>(executor);

			for(int i=0 ; i<n; i++) {
				final int ii = i;
				futures.add(completionService.submit(new Callable<Double>() {

					@Override
					public Double call() throws Exception {
						// select example i
						LabeledObject<X, Y> example = data.get(ii);			
						X x = example.getPattern();
						Y y = example.getLabel();

						// loss augmented inference
						Y yStar = loss.maxOracle(model, x, y);

						// computes sub-gradient term of the cutting plane model
						VectorOps.sub(at, loss.computeGradient(model, x, y, yStar));

						// return the loss
						return loss.error(model, y, yStar);
					}
				}));
			}

			for(Future<Double> f : futures) {
				try {
					// sum loss of each example
					bt += f.get();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
			}
			executor.shutdown();
		}

		// normalize by the number of examples
		if (n != 0) {
			VectorOps.mul(at, 1. /(double) n);
			bt /= n;
		}

		CuttingPlaneModel cuttingPlaneModel = new CuttingPlaneModel(at, bt);
		return cuttingPlaneModel;
	}

	public M optimize(final M model, final List<LabeledObject<X, Y>> data) {

		// define solver parameters
		final double c = options.getC();
		final int maxCuttingPlanes = options.getMaxCuttingPlanes();
		final int minCuttingPlanes = options.getMinCuttingPlanes();
		final double epsilon = options.getEpsilon();
		final int nThreads = options.getnThreads();
		final int nThreadsMosek = options.getnThreadsMosek();
		final int verbose = options.getVerbose();
		final int debugMultiplier = options.getDebugMultiplier();

		// define data parameters
		// n: number of examples
		final int n = data.size();
		// d: dimension of the feature space
		final int d = loss.getDimension(model, data.get(0).getPattern(), data.get(0).getLabel());

		// initialize model
		if (model.getDimension() == 0) {
			// initialize the weights to zero if there are not initialized
			model.setWeights(new double[d]);
		}

		if (verbose >= 0) System.out.println("Training with " + n + " examples of dimension " + d);

		List<CuttingPlaneModel> listOfCuttingPlaneModels = new ArrayList<CuttingPlaneModel>();
		List<Double> lc = new ArrayList<Double>();

		// compute initial cutting plane model
		CuttingPlaneModel cuttingPlaneModel = computeCuttingPlaneModel(model, data, nThreads);
		listOfCuttingPlaneModels.add(cuttingPlaneModel);
		double[] at = cuttingPlaneModel.getSubgradient();
		double bt = cuttingPlaneModel.getConstant();
		lc.add(bt);

		double[][] gram = null;
		double xi = 0.0;

		ProgressBar pb = null; 
		if (verbose == 1) {
			pb = new ProgressBar("Optimization", maxCuttingPlanes, 50);
			pb.start();
		}

		int t=0;
		while(t<minCuttingPlanes || (t<=maxCuttingPlanes && VectorOps.dot(model.getWeights(), at) < bt - xi - epsilon)) {

			//System.out.println("bt=" + bt + "\txi=" + xi + "\tdot=" + model.valueOf(at) + " < " + (bt - xi - epsilon));

			if(gram != null) {
				double[][] g = gram;
				gram = new double[listOfCuttingPlaneModels.size()][listOfCuttingPlaneModels.size()];
				for(int i=0; i<g.length; i++) {
					for(int j=0; j<g.length; j++) {
						gram[i][j] = g[i][j];
					}
				}
				for(int i=0; i<listOfCuttingPlaneModels.size(); i++) {
					gram[listOfCuttingPlaneModels.size()-1][i] = VectorOps.dot(listOfCuttingPlaneModels.get(listOfCuttingPlaneModels.size()-1).getSubgradient(), listOfCuttingPlaneModels.get(i).getSubgradient());
					gram[i][listOfCuttingPlaneModels.size()-1] = gram[listOfCuttingPlaneModels.size()-1][i];
				}
				gram[listOfCuttingPlaneModels.size()-1][listOfCuttingPlaneModels.size()-1] += eps;
			}
			else {
				gram = new double[listOfCuttingPlaneModels.size()][listOfCuttingPlaneModels.size()];
				for(int i=0; i<gram.length; i++) {
					for(int j=i; j<gram.length; j++) {
						gram[i][j] = VectorOps.dot(listOfCuttingPlaneModels.get(i).getSubgradient(), listOfCuttingPlaneModels.get(j).getSubgradient());
						gram[j][i] = gram[i][j];
						if(i==j) {
							gram[i][j] += eps;
						}
					}
				}
			}
			double[] alphas = MosekSolver.solveQP(gram, lc, c, nThreadsMosek);
			xi = (VectorOps.dot(alphas, lc.toArray(new Double[lc.size()])) - MatrixOps.product(alphas,gram))/c;
			//if (xi < 1e-8) xi = 0;

			if(verbose > 2) {
				System.out.println("alphas= " + Arrays.toString(alphas));
				System.out.println("DualObj= " + (VectorOps.dot(alphas,lc.toArray(new Double[lc.size()])) - 0.5 * MatrixOps.product(alphas,gram)));
			}

			// Compute the new model
			double[] w = new double[model.getDimension()];
			for(int i=0; i<alphas.length; i++) {
				double[] gradientCp = listOfCuttingPlaneModels.get(i).getSubgradient();
				for(int j=0; j<d; j++) {
					w[j] += alphas[i] * gradientCp[j];
				}
			}
			model.setWeights(w);
			t++;

			if (verbose > 1 && t % debugMultiplier == 0) {
				SolverUtils<X, Y, M> util = new SolverUtils<X, Y, M>();
				double primal = util.primalObjective(model, data, loss, 1.0, c);
				System.out.println("epoch=" + t + " \t primal=" + primal);
			}

			cuttingPlaneModel = computeCuttingPlaneModel(model, data, nThreads);
			listOfCuttingPlaneModels.add(cuttingPlaneModel);
			at = cuttingPlaneModel.getSubgradient();
			bt = cuttingPlaneModel.getConstant();
			lc.add(bt);

			if (verbose == 1) pb.step(); // increment the progress bar
		}
		if (verbose == 1) pb.stop(); // stop the progress bar

		return model;
	}

}
