package fr.lip6.durandt.wsl.util.solver;

import fr.lip6.durandt.wsl.util.model.IPrimalModel;

public interface StructuralLoss<X, Y, M extends IPrimalModel> extends Loss<X, Y, M> {

	/**
	 * a = a + psi(x,y)
	 * @param model
	 * @param a
	 * @param x
	 * @param y
	 */
	public void add(M model, double[] a, X x, Y y);

	/**
	 * a = a + gamma * psi(x,y)
	 * @param model
	 * @param a
	 * @param x
	 * @param y
	 */
	public void add(M model, double[] a, X x, Y y, double gamma);
	
	public double[] computeGradient(M model, X x, Y y, Y yStar);

	public double error(M model, Y yTruth, Y y);

	public Y maxOracle(M model, X x, Y yStar);

	/**
	 * a = a - psi(x,y)
	 * @param model
	 * @param a
	 * @param x
	 * @param y
	 */
	public void sub(M model, double[] a, X x, Y y);

}
