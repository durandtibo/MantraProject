package fr.lip6.durandt.wsl.util.model;

import fr.lip6.durandt.wsl.util.VectorOps;

public interface IPrimalModel {

	public int getDimension();

	public double[] getWeights();
	
	/**
	 * @return ||w||^2
	 */
	public default double norm2() {
		return VectorOps.dot(getWeights(), getWeights());
	}

	public void setWeights(double[] weights);
	
}
