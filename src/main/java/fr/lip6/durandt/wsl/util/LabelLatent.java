package fr.lip6.durandt.wsl.util;

import java.io.Serializable;

public class LabelLatent<Y,H> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7059525498896359041L;

	private Y label;

	private H latent;

	public LabelLatent(Y label, H latent) {
		super();
		this.label = label;
		this.latent = latent;
	}

	public Y getLabel() {
		return label;
	}

	public H getLatent() {
		return latent;
	}

	public void setLabel(Y label) {
		this.label = label;
	}

	public void setLatent(H latent) {
		this.latent = latent;
	}

	@Override
	public String toString() {
		return "LabelLatent [label=" + label + ", latent=" + latent + "]";
	}
	
	
}
