package fr.lip6.durandt.wsl.util.data.bag;

import java.io.File;
import java.io.IOException;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class DoubleBagReader {

	/**
	 * Convert the string of the feature in an array of double. 
	 * Verify if the dimension is correct with the provide dimension (dim).
	 * @param s string of the feature
	 * @param dim dimension of the feature
	 * @return feature in an array of double
	 */
	protected static double[] readFeature(String s, int dim, File file) {
		StringTokenizer st = new StringTokenizer(s);
		if(st.countTokens() != dim) {
			throw new RuntimeException("Error in readFeature for file " + file.getAbsolutePath() 
			+ " - incorrect number of tokens " + st.countTokens() + " != " + dim);
		}
		// Convert the string in array of double
		double[] feature = new double[dim];
		for(int i=0; i<dim; i++) {
			feature[i] = Double.parseDouble(st.nextToken());
		}
		return feature;
	}

	/**
	 * Read the XML file of a bag
	 * @param file
	 * @param dim desired dimension feature (if dim is negative, there is no verification of the feature dimension)
	 * @param verbose
	 */
	public static DoubleBag readXMLFile(File file, int dim, int maxNumberOfInstances, int verbose) {

		if(file.exists()) {

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

			DoubleBag bag = new DoubleBag();

			try {
				DocumentBuilder builder = factory.newDocumentBuilder();
				Document document = builder.parse(file);

				Element racine = document.getDocumentElement();
				NodeList racineNoeuds = racine.getChildNodes();
				int nbRacineNoeuds = racineNoeuds.getLength();

				// Number of instances
				int numberOfInstances = -1;

				for(int i=0; i<nbRacineNoeuds; i++) {
					if(racineNoeuds.item(i).getNodeType() == Node.ELEMENT_NODE) {
						Element e = (Element) racineNoeuds.item(i);
						if(verbose > 2) System.out.println(i + "\t" + e + "\t" + e.getTextContent());

						if(e.getTagName().compareToIgnoreCase("name") == 0) {
							// Read the name of the bag
							String name = e.getTextContent();
							bag.setName(name);
						}
						else if(e.getTagName().compareToIgnoreCase("numberofinstances") == 0) {
							// Read the number of instances
							numberOfInstances = Integer.parseInt(e.getTextContent());
						}
						else if(e.getTagName().compareToIgnoreCase("instance") == 0) {
							// Read an instance

							NodeList childNoeuds = e.getChildNodes();
							int nbChildNoeuds = childNoeuds.getLength();

							// Index of instance
							int index = -1;

							// Instance feature
							double[] feature = null;

							for(int j=0; j<nbChildNoeuds; j++) {
								if(childNoeuds.item(j).getNodeType() == Node.ELEMENT_NODE) {
									Element childElement = (Element) childNoeuds.item(j);

									if(verbose > 2) System.out.println(j + "\t" + childElement);

									if(childElement.getTagName().compareToIgnoreCase("index") == 0) {
										// Read index of instance
										index = Integer.parseInt(childElement.getTextContent());
									}
									else if(childElement.getTagName().compareToIgnoreCase("feature") == 0) {
										// Read instance feature
										int featureDim = Integer.parseInt(childElement.getAttribute("dim"));
										if(dim >= 0 && featureDim != dim) {
											System.out.println("ERROR - read dimension feature " + featureDim + " vs " + dim);
											System.exit(0);
										}
										String stringFeature = childElement.getTextContent();
										feature = readFeature(stringFeature, featureDim, file);
									}
								}
							}

							if(index != -1) {
								bag.addInstance(index, feature);
							}
							else {
								bag.addInstance(feature);
							}
						}
					}				
				}

				if(numberOfInstances != -1 && numberOfInstances != bag.getNumberOfInstances()) {
					throw new RuntimeException("Error in readXMLFile - incorrect number of instances " + numberOfInstances + " != " + bag.getNumberOfInstances());
				}
			}
			catch (final ParserConfigurationException e) {
				e.printStackTrace();
			}
			catch (final SAXException e) {
				e.printStackTrace();
			}
			catch (final IOException e) {
				e.printStackTrace();
			}

			return bag;
		}
		else {
			throw new RuntimeException("Error file " + file.getAbsolutePath() + " does not exist");
		}
	}

}
