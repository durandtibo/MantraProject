package fr.lip6.durandt.wsl.util.solver.bcfw;

import fr.lip6.durandt.wsl.util.solver.SolverOptions;

public class BCFWOptions extends SolverOptions {

	public static class Builder extends SolverOptions.Builder {

		/**
		 * regularization parameter
		 */
		private double lambda = 1e-4;

		private int numEpochs = 50;

		private int debugMultiplier = 1;

		private BCFWSample sample = BCFWSample.PERM;

		private boolean doWeightedAveraging = false;

		private boolean doLineSearch = true;


		public BCFWOptions build() {
			return new BCFWOptions(this);
		}

		public Builder debugMultiplier(int debugMultiplier) {
			this.debugMultiplier = debugMultiplier;
			return this;
		}

		public Builder doLineSearch(boolean doLineSearch) {
			this.doLineSearch = doLineSearch;
			return this;
		}

		public Builder doWeightedAveraging(boolean doWeightedAveraging) {
			this.doWeightedAveraging = doWeightedAveraging;
			return this;
		}

		public Builder lambda(double lambda) {
			this.lambda = lambda;
			return this;
		}

		public Builder numEpochs(int numEpochs) {
			this.numEpochs = numEpochs;
			return this;
		}

		public Builder sample(BCFWSample sample) {
			this.sample = sample;
			return this;
		}

	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 3126849295403393775L;

	private double lambda;

	private int numEpochs;

	private int debugMultiplier;

	private BCFWSample sample;

	private boolean doWeightedAveraging;

	private boolean doLineSearch;

	public BCFWOptions() {
		this(new BCFWOptions.Builder());
	}

	protected BCFWOptions(Builder builder) {
		super(builder);
		this.lambda = builder.lambda;
		this.numEpochs = builder.numEpochs;
		this.sample = builder.sample;
		this.doWeightedAveraging = builder.doWeightedAveraging;
		this.doLineSearch = builder.doLineSearch;
		this.debugMultiplier = builder.debugMultiplier;
	}

	public BCFWOptions(
			double lambda,
			int numEpochs, 
			BCFWSample sample, 
			boolean doWeightedAveraging,
			boolean doLineSearch,
			int debugMultiplier,
			int verbose,
			int seed) {

		super(verbose, seed);
		this.lambda = lambda;
		this.numEpochs = numEpochs;
		this.sample = sample;
		this.doWeightedAveraging = doWeightedAveraging;
		this.doLineSearch = doLineSearch;
		this.debugMultiplier = debugMultiplier;
	}

	public int getDebugMultiplier() {
		return debugMultiplier;
	}


	public double getLambda() {
		return lambda;
	}


	public int getNumEpochs() {
		return numEpochs;
	}

	public BCFWSample getSample() {
		return sample;
	}

	public boolean isDoLineSearch() {
		return doLineSearch;
	}

	public boolean isDoWeightedAveraging() {
		return doWeightedAveraging;
	}

	public void print() {
		StringBuilder sb = new StringBuilder();
		sb.append("# Block-Coordinate Frank-Wolfe options\n");
		sb.append("# lambda=").append(lambda).append("\n");
		sb.append("# sample=").append(sample).append("\n");
		sb.append("# numEpochs=").append(numEpochs).append("\n");
		sb.append("# doWeightedAveraging=").append(doWeightedAveraging).append("\n");
		sb.append("# doLineSearch=").append(doLineSearch).append("\n");
		System.out.print(sb.toString());
		super.print();
	}

	public void setDebugMultiplier(int debugMultiplier) {
		this.debugMultiplier = debugMultiplier;
	}

	public void setDoLineSearch(boolean doLineSearch) {
		this.doLineSearch = doLineSearch;
	}

	public void setDoWeightedAveraging(boolean doWeightedAveraging) {
		this.doWeightedAveraging = doWeightedAveraging;
	}

	public void setLambda(double lambda) {
		this.lambda = lambda;
	}

	public void setNumEpochs(int numEpochs) {
		this.numEpochs = numEpochs;
	}

	public void setSample(BCFWSample sample) {
		this.sample = sample;
	}

}
