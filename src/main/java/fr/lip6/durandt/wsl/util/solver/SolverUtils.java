package fr.lip6.durandt.wsl.util.solver;

import java.util.List;

import fr.lip6.durandt.wsl.util.LabeledObject;
import fr.lip6.durandt.wsl.util.model.IPrimalModel;

public class SolverUtils<X, Y, M extends IPrimalModel> {

	/**
	 * 1/n sum_i=1^n max_(y,h) delta(yi,y,h) + w^T psi(xi,y,h) - max_hstar w^T psi(xi,yi,hstar)	
	 * @param model
	 * @param data
	 * @return
	 */
	public double loss(final M model,
			final List<LabeledObject<X, Y>> data,
			final Loss<X, Y, M> loss) {

		double lossValue = 0;
		for(LabeledObject<X, Y> lo : data) {
			X x = lo.getPattern();
			Y y = lo.getLabel();

			lossValue += loss.evaluate(model, x, y);
		}
		if (data.size() != 0) lossValue /= (double) data.size();

		return lossValue;	
	}

	public double primalObjective(final M model,
			final List<LabeledObject<X, Y>> data,
			final Loss<X, Y, M> loss,
			final double lambda,
			final double c) {

		double lossValue = loss(model, data, loss);
		return 0.5 * lambda * model.norm2() + c * lossValue;	

	}

}
