package fr.lip6.durandt.wsl.util.data;

import java.util.ArrayList;
import java.util.List;

import fr.lip6.durandt.wsl.util.LabeledObject;
import fr.lip6.durandt.wsl.util.VectorOps;
import fr.lip6.durandt.wsl.util.data.bag.DoubleBag;

public class PreProcessingDoubleBag<Y> {

	public List<LabeledObject<double[], Y>> bagToArray(List<LabeledObject<DoubleBag, Y>> data) {
		List<LabeledObject<double[], Y>> newData = new ArrayList<LabeledObject<double[], Y>>();
		for(LabeledObject<DoubleBag, Y> lo : data) {
			for(int i=0; i<lo.getPattern().getNumberOfInstances(); i++) {
				newData.add(new LabeledObject<double[], Y>(lo.getPattern().getInstanceRepresentation(i).clone(), lo.getLabel()));
			}
		}
		return newData;
	}

	public List<LabeledObject<DoubleBag, Y>> normalization(List<LabeledObject<DoubleBag, Y>> data) {
		return normalization(data, true, true);
	}

	public List<LabeledObject<DoubleBag, Y>> normalization(List<LabeledObject<DoubleBag, Y>> data, boolean norm2, boolean bias) {

		for(LabeledObject<DoubleBag, Y> ls : data) {
			for(int i=0; i<ls.getPattern().getNumberOfInstances(); i++) {
				// L2 normalization of the instance
				if(norm2) {
					VectorOps.normL2(ls.getPattern().getInstance(i));
				}
				// Add a constant 1 feature for the bias
				if(bias) {
					ls.getPattern().setInstance(i,VectorOps.addValeur(ls.getPattern().getInstance(i),1));
				}
			}
		}

		return data;
	}

}
