package fr.lip6.durandt.wsl.util.data;

import java.util.List;

import fr.lip6.durandt.wsl.util.LabeledObject;
import fr.lip6.durandt.wsl.util.VectorOps;

public class PreProcessingData<Y> {

	public List<LabeledObject<double[], Y>> normalization(List<LabeledObject<double[], Y>> data) {
		return normalization(data, true, true);
	}

	public List<LabeledObject<double[], Y>> normalization(List<LabeledObject<double[], Y>> data, boolean norm2, boolean bias) {

		for(LabeledObject<double[], Y> ls : data) {
			// L2 normalization of the instance
			if(norm2) {
				VectorOps.normL2(ls.getPattern());
			}
			// Add a constant 1 feature for the bias
			if(bias) {
				ls.setPattern(VectorOps.addValeur(ls.getPattern(), 1.0));
			}
		}

		return data;
	}

}
