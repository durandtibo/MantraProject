package fr.lip6.durandt.wsl.util.solver.ssg;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import fr.durandt.progressbar.ProgressBar;
import fr.lip6.durandt.wsl.util.LabeledObject;
import fr.lip6.durandt.wsl.util.VectorOps;
import fr.lip6.durandt.wsl.util.model.IPrimalModel;
import fr.lip6.durandt.wsl.util.solver.StructuralLoss;

public class SSGSolver<X, Y, M extends IPrimalModel> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7408592731956526252L;

	private StructuralLoss<X, Y, M> loss = null;

	private SSGOptions options = null;

	public SSGSolver(StructuralLoss<X, Y, M> loss) {
		this(loss, new SSGOptions());
	}

	public SSGSolver(StructuralLoss<X, Y, M> loss, SSGOptions options) {
		super();
		this.loss = loss;
		this.options = options;
	}

	public M optimize(final M model, final List<LabeledObject<X, Y>> data) {

		// Define solver parameters
		final double lambda = options.getLambda();
		final SSGSample mode = options.getSample();
		final int numEpochs = options.getNumEpochs();
		final boolean doWeightedAveraging = options.isDoWeightedAveraging();
		final int seed = options.getSeed();
		final int verbose = options.getVerbose();
		final int debugMultiplier = options.getDebugMultiplier();

		// initialize random generator
		final Random randnum = new Random(seed);


		// define data parameters
		// n: number of examples
		final int n = data.size(); 
		// d: dimension of the feature space
		final int d = loss.getDimension(model, data.get(0).getPattern(), data.get(0).getLabel());

		if (verbose >= 0) System.out.println("Training with " + n + " examples of dimension " + d);

		// initialize model
		if (model.getDimension() == 0) {
			// initialize the weights to zero if there are not initialized
			model.setWeights(new double[d]);
		}

		// initialization in case of Weighted Averaging
		double[] wAvg = null;
		if(doWeightedAveraging) {
			wAvg = new double[d];
		}

		// generate the list of index
		List<Integer> listIndex = new ArrayList<Integer>(n);
		for(int i=0; i<n; i++) {
			listIndex.add(i);
		}
		if(SSGSample.PERM.equals(mode)) Collections.shuffle(listIndex, randnum);	// shuffle the list of examples

		// print information each debugIter
		int debugIter = debugMultiplier == 0 ? n : debugMultiplier;

		// initialize progress bar
		ProgressBar pb = null; 
		if (verbose == 1) {
			pb = new ProgressBar("Optimization", numEpochs, 50);
			pb.start();
		}

		int k = 0;
		for(int epoch=0; epoch<numEpochs; epoch++) {

			if(SSGSample.PERM.equals(mode)) Collections.shuffle(listIndex, randnum);	// shuffle the list of examples

			for(Integer i : listIndex) {
				if(SSGSample.UNIFORM.equals(mode)) i = randnum.nextInt(n); // select a random example

				// get example i
				LabeledObject<X, Y> lo = data.get(i);
				X x = lo.getPattern();
				Y y = lo.getLabel();

				// solve loss-augmented inference
				Y yStar = loss.maxOracle(model, x, y);

				// get sub-gradient				
				double[] gradient = loss.computeGradient(model, x, y, yStar);

				// step size gamma
				double gamma = 1.0 / (k + 1.0);

				// update the weights of the model
				VectorOps.add(model.getWeights(), gradient, 1 - gamma, - gamma / lambda);

				if (verbose > 2 && k % debugIter == 0) {
					System.out.print("epoch=" + epoch + " \t iter=" + k + " \t ");
				}

				k++;

				// update averaged weights
				if (doWeightedAveraging) {
					double rho = 2.0 / ((double)k + 2.0);
					VectorOps.add(wAvg, model.getWeights(), (1.0 - rho), rho);
				}
			}
			if (verbose == 1) pb.step(); // increment the progress bar
		}
		if (verbose == 1) pb.stop(); // stop the progress bar

		if (doWeightedAveraging) {
			model.setWeights(wAvg);
		}


		return model;
	}

}
