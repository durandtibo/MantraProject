package fr.lip6.durandt.demo;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import fr.lip6.durandt.wsl.mantra.Mantra;
import fr.lip6.durandt.wsl.mantra.model.RankAPMantraModel4DoubleBag;
import fr.lip6.durandt.wsl.mantra.solver.cuttingplane.MantraWith1SCP;
import fr.lip6.durandt.wsl.util.DatasetReaderXML;
import fr.lip6.durandt.wsl.util.LabeledObject;
import fr.lip6.durandt.wsl.util.data.PreProcessingDoubleBag;
import fr.lip6.durandt.wsl.util.data.bag.DoubleBag;
import fr.lip6.durandt.wsl.util.data.ranking.RankingLabel;
import fr.lip6.durandt.wsl.util.data.ranking.RankingPattern;
import fr.lip6.durandt.wsl.util.solver.cuttingplane.OneSCPOptions;

public class DemoRankAPMantra {

	public static void main(String[] args) {

		int dim = 4096;

		File fileTrain = new File("train.xml");
		File fileTest = new File("test.xml");

		PreProcessingDoubleBag<Integer> preprocess = new PreProcessingDoubleBag<Integer>();

		// read train data
		List<LabeledObject<DoubleBag, Integer>> list = DatasetReaderXML.readDoubleBag(fileTrain, dim, -1, 0);
		System.out.println("Data normalization");
		preprocess.normalization(list);
		RankingPattern<DoubleBag> rankPatternTrain = new RankingPattern<DoubleBag>(list);
		RankingLabel labelTrain = new RankingLabel(rankPatternTrain.getLabels());
		List<LabeledObject<RankingPattern<DoubleBag>, RankingLabel>> train = new ArrayList<LabeledObject<RankingPattern<DoubleBag>, RankingLabel>>();
		train.add(new LabeledObject<RankingPattern<DoubleBag>, RankingLabel>(rankPatternTrain, labelTrain));


		// read test data
		list = DatasetReaderXML.readDoubleBag(fileTest, dim, -1, 0);
		System.out.println("Data normalization");
		preprocess.normalization(list);
		RankingPattern<DoubleBag> rankPatternTest = new RankingPattern<DoubleBag>(list);
		RankingLabel labelTest = new RankingLabel(rankPatternTest.getLabels());
		List<LabeledObject<RankingPattern<DoubleBag>, RankingLabel>> test = new ArrayList<LabeledObject<RankingPattern<DoubleBag>, RankingLabel>>();
		test.add(new LabeledObject<RankingPattern<DoubleBag>, RankingLabel>(rankPatternTest, labelTest));

		// define model
		RankAPMantraModel4DoubleBag model = new RankAPMantraModel4DoubleBag();

		// define solver options
		OneSCPOptions options = new OneSCPOptions();
		options.setC(1e4);
		options.setEpsilon(1e-2);
		options.setMaxCuttingPlanes(500);
		options.setnThreads(1);

		// define solver and train it
		MantraWith1SCP<RankingPattern<DoubleBag>, RankingLabel, List<Integer>> solver = new MantraWith1SCP<RankingPattern<DoubleBag>, RankingLabel, List<Integer>>(options);
		Mantra<RankingPattern<DoubleBag>, RankingLabel, List<Integer>> learner = new Mantra<RankingPattern<DoubleBag>, RankingLabel, List<Integer>>(solver);

		learner.train(model, train, test);

	}

}
