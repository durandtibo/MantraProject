package fr.lip6.durandt.demo;

import java.io.File;
import java.util.List;

import fr.lip6.durandt.wsl.mantra.Mantra;
import fr.lip6.durandt.wsl.mantra.model.MultiClassMantraModel4DoubleBag;
import fr.lip6.durandt.wsl.mantra.solver.cuttingplane.MantraWith1SCP;
import fr.lip6.durandt.wsl.util.DatasetReaderXML;
import fr.lip6.durandt.wsl.util.LabeledObject;
import fr.lip6.durandt.wsl.util.data.PreProcessingDoubleBag;
import fr.lip6.durandt.wsl.util.data.bag.DoubleBag;
import fr.lip6.durandt.wsl.util.solver.cuttingplane.OneSCPOptions;

public class DemoMultiClassMantra {

	public static void main(String[] args) {
		
		// feature dimension
		int dim = 4096;

		File fileTrain = new File("train.xml");
		File fileTest = new File("test.xml");

		PreProcessingDoubleBag<Integer> preprocess = new PreProcessingDoubleBag<Integer>();

		// read train data
		List<LabeledObject<DoubleBag, Integer>> train = DatasetReaderXML.readDoubleBag(fileTrain, dim, -1, 0);
		System.out.println("Data normalization");
		preprocess.normalization(train);


		// read test data
		List<LabeledObject<DoubleBag, Integer>> test = DatasetReaderXML.readDoubleBag(fileTest, dim, -1, 0);
		System.out.println("Data normalization");
		preprocess.normalization(test);

		// define model
		MultiClassMantraModel4DoubleBag model = new MultiClassMantraModel4DoubleBag();

		// define solver options
		OneSCPOptions options = new OneSCPOptions();
		options.setC(1e4);
		options.setEpsilon(1e-2);
		options.setMaxCuttingPlanes(500);
		options.setnThreads(1);

		// define solver and train it
		MantraWith1SCP<DoubleBag, Integer, Integer> solver = new MantraWith1SCP<DoubleBag, Integer, Integer>(options);
		Mantra<DoubleBag, Integer, Integer> learner = new Mantra<DoubleBag, Integer, Integer>(solver);

		learner.train(model, train, test);

	}

}
